#!/usr/bin/env python
"""
The Pozyx ready to range tutorial (c) Pozyx Labs
Please read the tutorial that accompanies this sketch: https://www.pozyx.io/Documentation/Tutorials/ready_to_range/Python

This demo requires two Pozyx devices and one Arduino. It demonstrates the ranging capabilities and the functionality to
to remotely control a Pozyx device. Place one of the Pozyx shields on the Arduino and upload this sketch. Move around
with the other Pozyx device.

This demo measures the range between the two devices. The closer the devices are to each other, the more LEDs will
light up on both devices.
"""
from pypozyx import *
from lifxlan import *

if __name__ == "__main__":
    port = '/dev/ttyACM0'               # COM port of the Pozyx device
    destination_id = 0x6068      # network ID of the ranging destination
    lifxlan = LifxLAN()
    lights =  lifxlan.get_lights()

    pozyx = PozyxSerial(port)
    while True:
        device_range = DeviceRange()
        status = pozyx.doRanging(destination_id, device_range)
        if status == POZYX_SUCCESS:
            for light in lights:
                light.set_color([ device_range[1]*10 ,65535 ,32500, 9000 ], rapid= True)
                print(device_range[1]*10)
        else:
            print("ERROR: ranging")
